const send = (message, receiver) => {
    chrome.runtime.sendMessage(message, receiver);
};

const message = {
    send
};

export default message;