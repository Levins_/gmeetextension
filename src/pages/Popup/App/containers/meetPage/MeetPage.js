import React from "react";
import { Tabs, Row } from 'antd';
import { PlusCircleTwoTone, ProfileTwoTone } from '@ant-design/icons';
import MeetingCreateForm from "../MeetingCreateForm/MeetingCreateForm";
import MeetingList from "../MeetingList/MeetingList";
const { TabPane } = Tabs;

const MeetPage = (props) => {
    return (
        <Row style={{ height: 'inherit' }}>
            <Tabs defaultActiveKey="1" style={{ width: '100%' }}>
                <TabPane
                    tab={
                        <span>
                            <PlusCircleTwoTone twoToneColor="#058effe8" />
                            Create Meeting
                        </span>
                    }
                    key="1"
                >
                    <MeetingCreateForm />
                </TabPane>
                <TabPane
                    tab={
                        <span>
                            <ProfileTwoTone twoToneColor="#058effe8" />
                            Meetings
                        </span>
                    }
                    key="2"
                >
                    <MeetingList />
                </TabPane>
            </Tabs>
        </Row>
    );
};

export default MeetPage;