import React, { useContext } from "react";
import { List } from 'antd';
import MeetingEventListItem from "../../components/MeetingEventListItem/MeetingEventListItem";
import MeetingContext from "../../context/meetingContext";
import LoadingPage from "../../components/LoadingPage/LoadingPage";

const MeetingList = (props) => {
    let { events, isMeetingCreateOnProgress } = useContext(MeetingContext);
    console.log(events, isMeetingCreateOnProgress);
    return (
        isMeetingCreateOnProgress ? <LoadingPage /> : <List
            itemLayout="horizontal"
            dataSource={events}
            renderItem={MeetingEventListItem}
        />
    );
};

export default MeetingList;