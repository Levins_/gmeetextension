import React, { useContext, useState } from 'react';
import { Form, Input, DatePicker, TimePicker, Button, Alert, Result } from 'antd';
import moment from 'moment';
import ActionContext from '../../context/actionContext';
import MeetingContext from '../../context/meetingContext';
import { LinkOutlined } from '@ant-design/icons';

import { Typography } from 'antd';
import GoogleMeetIcon from '../../components/IconGroup/GoogleMeetIcon';
const { Paragraph, Link } = Typography;


const formItemLayout = {
    labelCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 8,
        },
    },
    wrapperCol: {
        xs: {
            span: 24,
        },
        sm: {
            span: 16,
        },
    },
};
let roundTime = (time, minutesToRound) => {

    let [hours, minutes] = time.split(':');
    hours = parseInt(hours);
    minutes = parseInt(minutes);

    time = (hours * 60) + minutes;

    let rounded = Math.round(time / minutesToRound) * minutesToRound;
    let rHr = '' + Math.floor(rounded / 60)
    let rMin = '' + rounded % 60

    return rHr.padStart(2, '0') + ':' + rMin.padStart(2, '0')
}

const MeetingCreateForm = (props) => {
    let { onCreateMeeting, openCreateNewEventForm } = useContext(ActionContext);
    let { isMeetingCreateOnProgress, newCreatedEvent = {} } = useContext(MeetingContext);
    let {
        hangoutLink
    } = newCreatedEvent;
    const onFinish = (formValue) => {
        console.log(formValue);
        let {
            attendees,
            date,
            event_time_range
        } = formValue;
        const payload = {
            conferenceData: {
                createRequest: {
                    conferenceSolutionKey: {
                        type: 'hangoutsMeet'
                    },
                    requestId: "uig43uy5u3g3u4ygu3yruy4g"
                }
            },
            start: {
                dateTime: `${date ? date.format('YYYY-MM-DD') : currentDate.format('YYYY-MM-DD')}T${event_time_range ? event_time_range[0].subtract(5, 'h').subtract(30, 'm').format('HH:mm:ss[Z]') : timeRangeDefaultValue[0].subtract(5, 'h').subtract(30, 'm').format('HH:mm:ss[Z]')}`
            },
            end: {
                dateTime: `${date ? date.format('YYYY-MM-DD') : currentDate.format('YYYY-MM-DD')}T${event_time_range ? event_time_range[1].subtract(6, 'h').subtract(30, 'm').format('HH:mm:ss[Z]') : timeRangeDefaultValue[1].subtract(6, 'h').subtract(30, 'm').format('HH:mm:ss[Z]')}`
            }
        };
        if (attendees) {
            payload.attendees = attendees.split(',').map((attendee) => {
                return { email: attendee };
            });
        }
        onCreateMeeting(payload);
    };
    var todayTime = new Date().toISOString();
    var date = todayTime.split('T')[0];
    var time = todayTime.split('T')[1];

    let currentDate = moment(date, 'YYYY/MM/DD');
    let currentTime = moment(time, 'HH:mm:ss.SSS[Z]').add(5, 'h').add(30, 'm');
    let endTime = moment(time, 'HH:mm:ss.SSS[Z]').add(6, 'h').add(30, 'm');
    let timeRangeDefaultValue = [currentTime, endTime]
    return (
        <React.Fragment>
            {
                (hangoutLink) ? <Result
                    icon={<LinkOutlined />}
                    title="Meeting Event Created Successfully"
                    extra={<React.Fragment><Paragraph copyable={{ text: hangoutLink }} style={{ marginRight: 0 }}><Link href={hangoutLink} target="_blank">{hangoutLink}</Link></Paragraph><Button
                        type="primary"
                        icon={<GoogleMeetIcon emptyStyle={true} />}
                        style={{ display: 'inline-flex', padding: '10px 30px', height: 'auto' }} onClick={openCreateNewEventForm}>
                        <span style={{ marginLeft: '10px' }}>Create new meeting event</span>
                    </Button></React.Fragment>}
                    style={{
                        display: 'flex', flexDirection: 'column', alignItems: 'center', alignContent: 'center', justifyContent: 'center'
                    }}
                /> : <React.Fragment>
                    <Alert message={<React.Fragment><b>Note :</b> By creating the meeting from this extension will add the event on your loged in account calender.</React.Fragment>} type="info" style={{ marginBottom: '24px' }} />
                    <Form name="time_related_controls" {...formItemLayout} onFinish={onFinish}>
                        <Form.Item name="date" label="Date" tooltip="Meeting event date">
                            <DatePicker defaultValue={currentDate} />
                        </Form.Item>
                        <Form.Item name="event_time_range" label="Time Range" tooltip="Start time and end time of meeting." >
                            <TimePicker.RangePicker defaultValue={timeRangeDefaultValue} />
                        </Form.Item>
                        <Form.Item label="Attendees" name="attendees" tooltip="We will add the mentioned email users as attendees and will notify them on meeting date" help={"Please add the user's email as Comma separated"}>
                            <Input defaultValue={'levinsdurai.v@gmail.com,developer.levins@gmail.com'} />
                        </Form.Item>
                        <Form.Item
                            wrapperCol={{
                                xs: {
                                    span: 24,
                                    offset: 0,
                                },
                                sm: {
                                    span: 16,
                                    offset: 8,
                                },
                            }}
                        >
                            <Button type="primary" htmlType="submit" loading={isMeetingCreateOnProgress} style={{ marginTop: '25px' }}>Create Meeting</Button>
                        </Form.Item>
                    </Form>
                </React.Fragment>
            }
        </React.Fragment>
    );
};

export default MeetingCreateForm;