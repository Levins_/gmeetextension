import React from "react";
import 'antd/dist/antd.css';
import HeaderPanel from "./components/HeaderPanel/HeaderPanel";
import message from "./message";
import actions from "../../message.actions.constants";
import LoadingPage from "./components/LoadingPage/LoadingPage";
import { Row, Col } from 'antd';
import { Layout } from 'antd';
import LoginPage from "./components/LoginPage/LoginPage";
import ActionContext from "./context/actionContext";
import MeetPage from "./containers/meetPage/MeetPage";
import MeetingContext from "./context/meetingContext";

const { Content } = Layout;

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoadingPage: false,
            isTokenFetchOnProgress: false,
            isUserInfoOnFetch: false,
            isMeetingCreateOnProgress: false
        };
        this.messageReceiver = this.messageReceiver.bind(this);
        this.loginOnGoogle = this.loginOnGoogle.bind(this);
        this.getUserInfo = this.getUserInfo.bind(this);
        this.onCreateMeeting = this.onCreateMeeting.bind(this);
        this.openCreateNewEventForm = this.openCreateNewEventForm.bind(this);
        this.getCachedMeetingList = this.getCachedMeetingList.bind(this);
        this.onLogout = this.onLogout.bind(this);
    }
    messageReceiver(response) {
        let {
            action,
            payload
        } = response;
        switch (action) {
            case actions.loginOnGoogle:
            case actions.fetchUserInfo: {
                let { user, isUserSessionNotFound, loginError } = payload;
                this.setState({
                    user,
                    isUserSessionNotFound,
                    loginError,
                    isUserInfoOnFetch: false
                });
                break;
            }
            case actions.createMeetingEvent: {
                let { event, isUserSessionNotFound } = payload;
                let { events = [] } = this.state;
                this.setState({
                    isMeetingCreateOnProgress: false,
                    newCreatedEvent: event,
                    events: [event, ...events],
                    isUserSessionNotFound
                });
                break;
            }
            case actions.fetchCachedMeetings: {
                let { events } = payload;
                this.setState({
                    isMeetingListOnFetch: false,
                    events
                });
                break;
            }
            case actions.logOutFromExtention: {
                this.setState({
                    isUserSessionNotFound: true,
                    user: undefined
                });
                break;
            }
        }
    }
    loginOnGoogle() {
        this.setState({ isLoadingPage: true }, () => {
            message.send({
                action: actions.loginOnGoogle
            }, this.messageReceiver);
        });
    }
    getUserInfo() {
        this.setState({ isUserInfoOnFetch: true }, () => {
            message.send({
                action: actions.fetchUserInfo
            }, this.messageReceiver);
        });
    }
    openCreateNewEventForm() {
        this.setState({
            isMeetingCreateOnProgress: false,
            newCreatedEvent: {}
        })
    }
    onCreateMeeting(payload) {
        this.setState({
            isMeetingCreateOnProgress: true
        }, () => {
            message.send({
                action: actions.createMeetingEvent,
                payload
            }, this.messageReceiver);
        });
    }
    onLogout() {
        message.send({
            action: actions.logOutFromExtention
        }, this.messageReceiver);
    }
    getCachedMeetingList() {
        this.setState({
            isMeetingListOnFetch: true
        }, () => {
            message.send({
                action: actions.fetchCachedMeetings
            }, this.messageReceiver);
        });
    }
    componentDidMount() {
        this.getUserInfo();
        this.getCachedMeetingList();
    }
    render() {

        let {
            user,
            isUserSessionNotFound,
            loginError,
            isUserInfoOnFetch,

            isMeetingCreateOnProgress,
            newCreatedEvent,
            events
        } = this.state;
        console.log('user', user);
        return (
            <ActionContext.Provider value={{ loginOnGoogle: this.loginOnGoogle, onCreateMeeting: this.onCreateMeeting, openCreateNewEventForm: this.openCreateNewEventForm }}>
                <MeetingContext.Provider value={{ isMeetingCreateOnProgress, newCreatedEvent, events }}>
                    <Layout style={{ width: 'inherit', height: 'inherit' }}>
                        <Row>
                            <HeaderPanel user={user} onLogout={this.onLogout} />
                        </Row>
                        <Row style={{ height: 'inherit' }}>
                            <Content className="site-layout" style={{ padding: '0 50px', height: 'inherit' }}>
                                {
                                    isUserInfoOnFetch ? <LoadingPage /> : isUserSessionNotFound ? <LoginPage /> : <MeetPage />
                                }
                            </Content>
                        </Row>
                    </Layout>
                </MeetingContext.Provider>
            </ActionContext.Provider>
        );
    }
};