import React from "react";
import { Row, Col } from 'antd';
// import LoadingIcon from "../IconGroup/LoadingIcon";
import './LoadingPage.css';

const LoadingPage = (props) => {
    return <Row gutter={[16, 16]} style={{ height: 'inherit' }}>
        <Col span={24}>
            {/* <LoadingIcon /> */}
            <div className="loading">
                <div className="loading-text">
                    <span className="loading-text-words">L</span>
                    <span className="loading-text-words">O</span>
                    <span className="loading-text-words">A</span>
                    <span className="loading-text-words">D</span>
                    <span className="loading-text-words">I</span>
                    <span className="loading-text-words">N</span>
                    <span className="loading-text-words">G</span>
                </div>
            </div>
        </Col>
    </Row>
};

export default LoadingPage;