import { Avatar, Breadcrumb, List, Row, Tooltip } from "antd";
import moment from "moment";
import React from "react";
import { Typography } from 'antd';
const { Paragraph, Link } = Typography;
import './MeetingEventListItem.css';

// function generateRandomColor(str) {
//     var hash = 0;
//     for (var i = 0; i < str.length; i++) {
//         hash = str.charCodeAt(i) + ((hash << 5) - hash);
//     }

//     var h = hash % 360;
//     return `hsl(${h},60%,90%)`;
// };

const MeetingEventListItem = (props) => {
    let {
        end,
        start,
        attendees,
        hangoutLink,
        creator,
        created
    } = props;
    console
    let startDateTime = start ? new Date(start.dateTime) : '';
    let endDateTime = end ? new Date(end.dateTime) : '';
    return (
        <List.Item>
            <List.Item.Meta
                title={<Paragraph copyable={{ text: hangoutLink }}><Link href={hangoutLink} target="_blank">
                    {hangoutLink}
                </Link></Paragraph>}
                description={
                    <React.Fragment>
                        <Row>
                            {
                                `Meeting event Created by ${creator.email} on ${moment(created).format('DD:MM:YYYY hh:mm:ss A')}`
                            }
                        </Row>
                        <Row>
                            <span><b className="bold">Date : </b>{`${startDateTime.getDate()}.${startDateTime.getMonth()}.${startDateTime.getFullYear()}`}</span>
                            <Breadcrumb.Separator><span style={{ color: 'rgb(0 54 255 / 25%)' }}>{' | '}</span></Breadcrumb.Separator>
                            <span>
                                <b className="bold">Time : </b>
                                {
                                    start ? <React.Fragment><span>{`${startDateTime.getHours()}:${startDateTime.getMinutes()}:${startDateTime.getSeconds()}`}</span><Breadcrumb.Separator><span style={{ color: 'rgb(0 54 255 / 25%)' }}>{'>'}</span></Breadcrumb.Separator></React.Fragment> : null
                                }
                                {
                                    end ? <span>{`${endDateTime.getHours()}:${endDateTime.getMinutes()}:${endDateTime.getSeconds()}`}</span> : null
                                }
                            </span>
                        </Row>
                        {
                            attendees && attendees.length ? <Row style={{ display: 'flex', alignItems: 'flex-end' }}>
                                <span style={{ marginRight: '10px' }}><b className="bold">Attendees : </b></span>
                                <Avatar.Group>
                                    {
                                        attendees.map((attende) => {
                                            return <Tooltip title={attende.email} placement="top">
                                                <Avatar style={{ backgroundColor: `#${Math.floor(Math.random() * 16777215).toString(16)}` }} >{attende.email[0]}</Avatar>
                                            </Tooltip>
                                        })
                                    }
                                </Avatar.Group>
                            </Row> : null
                        }
                    </React.Fragment>
                }
            />
        </List.Item>
    );
};

export default MeetingEventListItem;