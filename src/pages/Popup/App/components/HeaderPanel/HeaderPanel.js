import React from "react";
import { Layout, Row, Col, Space, Typography, Avatar, Tooltip } from 'antd';
import GoogleMeetIcon from "../IconGroup/GoogleMeetIcon";
import './HeaderPanel.css';
const { Header } = Layout;
const { Title } = Typography;

const HeaderPanel = (props) => {
    let { user, onLogout } = props;
    return (
        <Header style={{ width: '100%' }}>
            <Row style={{ justifyContent: 'space-between' }}>
                <Col style={{ display: 'flex', alignItems: 'center', flexDirection: 'column', justifyContent: 'center' }}>
                    <GoogleMeetIcon /> <Title level={4} style={{ margin: 0, color: 'white' }}>G Meet</Title>
                </Col>
                <Col style={{
                    minWidth: '90px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                    fontSize: '15px',
                    fontWeight: 900
                }}>
                    {
                        user ? <span className="logourbutton" onClick={onLogout}>Logout</span> : null
                    }
                    {
                        user ? <Tooltip title={user.name} placement="top"><Avatar src={user.picture} /></Tooltip> : null
                    }
                </Col>
            </Row>
        </Header>
    );
};

export default HeaderPanel;