const { default: actions } = require("../message.actions.constants");
const { default: apiKeys } = require("./apikeys.production");

const setData = (payload) => {
    chrome.storage.local.set(payload);
};
const getData = (keys, callBack) => {
    chrome.storage.local.get(keys, callBack);
};
const removeData = (keys, callBack) => {
    chrome.storage.local.remove(keys, callBack);
};
const logOut = (sendRes) => {
    chrome.identity.clearAllCachedAuthTokens(sendRes);
};
const forceToLogin = (sendRes) => {
    chrome.identity.getAuthToken(
        { interactive: true },
        (token) => {
            if (token) {
                setData({ token });
                sendRes({ token });
            }
        }
    );

};

const getUserAuthToken = (sendRes) => {

    const fetchToken = (result) => {
        if (result && result.token) {
            sendRes({ token: result.token });
            console.log(result.token);
            fetchUserInfo(result.token);
        }
        else {
            forceToLogin(sendRes);
        }
    };

    getData(['token'], fetchToken);
};

const apiCaller = (url, reqBody) => {
    return fetch(url, reqBody).then((res) => {
        return res.json();
    });
}
const fetchUserInfo = (token, sendRes) => {
    apiCaller('https://www.googleapis.com/oauth2/v3/userinfo', {
        method: "GET",
        headers: {
            Authorization: `Bearer ${token}`
        }
    }).then((res = {}) => {
        let {
            name,
            picture
        } = res;
        const data = (name || picture) ? { user: { name, picture } } : null;
        sendRes(data);
    });
};
const getCachedUserAuthToken = (sendRes) => {
    const cachedTokenGetter = (result) => {
        if (result && result.token) {
            forceToLogin(sendRes);
        }
        else {
            sendRes({ isUserSessionNotFound: true });
        }
    };
    getData(['token'], cachedTokenGetter);
};

const createMeetingEvent = ({ token, payload }, sendRes) => {
    apiCaller(`https://www.googleapis.com/calendar/v3/calendars/primary/events?conferenceDataVersion=1&sendNotifications=true&sendUpdates=all&key=${apiKeys.gApiKey}`, {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        }
    }).then((res = {}) => {
        let { hangoutLink, start, end, creator, created, attendees } = res;
        let newEvent = {
            end,
            start,
            creator,
            created,
            hangoutLink,
            attendees
        };
        let oldValueGetter = (result) => {
            let { events = '[]' } = result;
            events = JSON.parse(events);
            events.unshift(newEvent);
            setData({ events: JSON.stringify(events) });
            sendRes({
                action: actions.createMeetingEvent,
                payload: {
                    event: newEvent
                }
            });
        };
        getData(['events'], oldValueGetter);
    });
};

const messageListener = (request, sender, sendResponse) => {
    let { action, payload } = request;
    console.log(action);
    switch (action) {
        case actions.fetchUserInfo: {
            const payloadGetter = (payload) => {
                console.log(payload);
                let { token } = payload;
                if (token) {
                    const sendRes = (data) => {
                        let { user } = data;
                        user ? sendResponse({ action: actions.fetchUserInfo, payload: data }) : sendResponse({ action: actions.fetchUserInfo, payload: { loginError: true } });
                    };
                    fetchUserInfo(token, sendRes);
                }
                else {
                    sendResponse({ action: actions.fetchUserInfo, payload: { isUserSessionNotFound: true } });
                }
            }
            getCachedUserAuthToken(payloadGetter);
            break;
        }
        case actions.loginOnGoogle: {
            const payloadGetter = (payload) => {
                let { token } = payload;
                if (token) {
                    const sendRes = (data) => {
                        let { user } = data;
                        user ? sendResponse({ action: actions.fetchUserInfo, payload: data }) : sendResponse({ action: actions.fetchUserInfo, payload: { loginError: true } });
                    };
                    fetchUserInfo(token, sendRes);
                }
                else {
                    sendResponse({ action: actions.fetchUserInfo, payload: { isUserSessionNotFound: true } });
                }
            }
            getUserAuthToken(payloadGetter);
            break;
        }
        case actions.createMeetingEvent: {
            const payloadGetter = (data) => {
                let { token } = data;
                if (token) {
                    createMeetingEvent({ token, payload }, sendResponse);
                }
                else {
                    // something went wrong
                }
            }
            getCachedUserAuthToken(payloadGetter);
            break;
        }
        case actions.fetchCachedMeetings: {
            const payloadGetter = (data) => {
                let { events = '[]' } = data;
                events = JSON.parse(events);
                sendResponse({ action: actions.fetchCachedMeetings, payload: { events } });
            };
            getData(['events'], payloadGetter);
            break;
        }
        case actions.logOutFromExtention: {
            console.log(actions.logOutFromExtention);
            logOut(() => {
                removeData(['token']);
                sendResponse({ action: actions.logOutFromExtention, payload: { success: true } });
            });
            break
        }
    }
    return true;
};



chrome.runtime.onMessage.addListener(messageListener);