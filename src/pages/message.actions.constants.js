const actions = {

    fetchUserInfo: 'fetchUserInfo',
    loginOnGoogle: 'loginOnGoogle',
    createMeetingEvent: 'createMeetingEvent',
    fetchCachedMeetings: 'fetchCachedMeetings',
    logOutFromExtention: 'logOutFromExtention'

};

export default actions;