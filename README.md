# G Meet
This chrome extension will help u to make an meeting event with attendees.

# Product images
![Login page](./assets/1.png) 
![Google Login page](./assets/2.png) 
![Meeting create form](./assets/3.png) 
![Meeting create on success](./assets/4.png) 
![Meetings list page](./assets/5.png) 

# Functionalities
* Login and managing google authentication for this extension
* Making an event with attendees and getting the instant Google meeting link
* Already created meeting link list page

# Installation
Clone Repo
```
    git clone https://gitlab.com/Levins_/gmeetextension.git
```

Go to `gmeetextension` directory and run
```
npm install
```

Now build the extension using
```
npm run start
```

Refer the below link to install the chrome extension on your browser 
https://developer.chrome.com/docs/extensions/mv3/getstarted/#unpacked


# References
* https://developers.google.com/calendar/api/v3/reference/events/insert
* https://developer.chrome.com/docs/extensions/reference/identity/